#request a name input from the player
userName = input("Hi! What is your name? ")
#set up loop to go thorugh a range of 5 guesses
for guessAttempt in range(5):

    #Import randint
    from random import randint
    #create a months variable
    guessMonth = randint(1, 12)
    #create a years variable
    guessYear = randint(1922, 2022)
    #Guess the players birthday month and year
    print(f"Guess{guessAttempt} :", userName , "were you born in ", guessMonth, "/", guessYear, "? ")
    #get input form player if guess was correct or not
    response = input("yes or no? ")
    #if yes
    if response == "yes":
        print("I knew it!")
        break
    #once we hit 5 attempts, give up and move on. game is over.
    elif guessAttempt == 4:
        print("I have other things to do. Goodbye.")
        break
    #if no
    else:
        print("Drat! Lemme try again!")
print("Thank you for playing!")
